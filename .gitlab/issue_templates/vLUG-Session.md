vLUG-Session zum Thema <Bezeichnung>

# Beschreibung
## Kurzbeschreibung
(Hier eine Kurzbeschreibung)
## Zielgruppe
(Welche Zielgruppe hat der Artikel)
### Inhalt
(Idee für Inhaltsverzeichnis oder Strichworte)
### Dauer
(Dauer der Session)

(Gibt es schon Personen die dran arbeiten wollen?)
/assign @devLUG

/label ~vLUG-Session

(Wähle einen Milestone)
/milestone %"devLUG-Sprint 2018-06"
/milestone %"Sprint-Backlog"

